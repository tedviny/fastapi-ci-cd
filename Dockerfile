FROM python:3.8-slim-buster

COPY ./app /app/app
COPY ./requirements.txt /app


WORKDIR /app

RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

EXPOSE 5000

CMD ["uvicorn", "app.main:app", "--host=0.0.0.0", "--reload"]