from starlette.testclient import TestClient
from app.main import app, db_enseignant


client = TestClient(app)


def test_root_endpoint():
    resp = client.get("/")
    assert resp.status_code == 200
    assert resp.json() == {"message": "Bienvenu sur notre API"}


def test_enseignants_endpoint():
    resp = client.get("/api/v1/enseignants")
    assert resp.status_code == 200


def test_enseignant_endpoint():
    resp = client.post("/api/v1/enseignant",
                       headers={"X-Token": "coneofsilence"},
                       json={'id': 1, 'nom': 'Doe',
                             'prenom': 'John', 'age': 23, 'cours': ['informatique' ]})
    assert resp.status_code == 200
    assert resp.json() == {
        "message": "Enseignant avec id : 1 du nom de : Doe a bien été ajouté"}
