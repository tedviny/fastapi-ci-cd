build: venv ## Build the project
	. venv/bin/activate \
	&& python app/main.py

run: venv ## Run uvicorn server with python
	. venv/bin/activate \
	&& python app/run.py 

test: venv ## unit test python
	. venv/bin/activate \
	&& pytest tests/endpoints_test.py

lint: venv  #Lint & format, will not fix but sets exit code on error 
	. venv/bin/activate \
	&& black --check \
	&& flake8 app/ && flake8 run.py

lint-fix: venv  # Lint & format, will try to fix errors and modify code
	. venv/bin/activate \
	&& black /

venv: venv/touchfile

venv/touchfile: requirements.txt
	python3 -m venv venv
	. venv/bin/activate; pip install -r requirements.txt
	touch venv/touchfile

clean:  #Clean up project
	rm -rf .venv
	rm -rf tests/package*
	rm -rf app/__pycache__
	rm -rf tests/__pycache__
	rm -rf tests/.pytest_cache

