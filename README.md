## 🚀 CI/CD SETUP Python FastAPI project with gitlab-ci

### Jobs

_build job_
_Test job_
_Build image job_
_Deploy job_

### Project running:

`uvicorn main:app --reload`

### Project testing:

`pytest tests`
